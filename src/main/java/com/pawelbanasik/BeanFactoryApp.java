package com.pawelbanasik;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class BeanFactoryApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		
		
		Organization organization = (Organization) context.getBean("myorg");
		Organization organization2 = (Organization) context.getBean("myorg2");
		
//		organization.corporateSlogan();

		System.out.println(organization);
		System.out.println(organization2);
		
		
		
		((AbstractApplicationContext) context).close();

		
		
	}
}
